import { collection, onSnapshot } from "@firebase/firestore";
import React, { useState, useEffect } from "react";
import { BrowserRouter, Route } from "react-router-dom";

import "./App.css";
import AuthenticationView from "./components/AuthenticationView";

import Home from "./components/Home";
import Hod from "./components/routes/Hod";
import Dvc from "./components/routes/Dvc";
import Admin from "./components/routes/Admin";
import Stuff from "./components/routes/Stuff";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Route exact path="/" component={Home} />
        <Route exact path="/auth" component={AuthenticationView} />
        <Route exact path="/stuff" component={Stuff} />
        <Route exact path="/hod" component={Hod} />
        <Route exact path="/dvc" component={Dvc} />
        <Route exact path="/admin" component={Admin} />
      </BrowserRouter>
    </div>
  );
}

export default App;
