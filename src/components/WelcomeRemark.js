import React from "react";
import "../css/welcomeRemark.css";

const WelcomeRemark = () => {
    return(
        <div className="container">
            <h1>Welcome User</h1>
            <p className="story1">
                Available to you below are the doorways to different
                functionalities in the workload sheet automation system.
            </p>
            <p className="story2">Only use the ones you are authorised to utilize.</p>
        </div>
    )
}

export default WelcomeRemark;