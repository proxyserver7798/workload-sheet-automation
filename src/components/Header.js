import React from "react";
import "../css/header.css";
import logo from "../resources/nust.png";

const Header = () => {
  return (
    <div className="brand">
      <div className="brand-logo">
        <img src={logo} alt="oic" />
      </div>
      <div className="brandRight">
        <h1>WORKLOAD SHEET AUTOMATION SYSTEM</h1>
        <h5>Designed with love</h5>
        <p>A gift from WIT</p>
      </div>
    </div>
  );
};

export default Header;
