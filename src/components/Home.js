import { collection, onSnapshot } from '@firebase/firestore';
import React, { useState, useEffect } from 'react';

import Header from "./Header";
import WelcomeRemark from "./WelcomeRemark";
import Departments from "./Departments";

import db from "../firebase"

const Home = () => {

    const [ departments, setDepartments ] = useState([]);

    useEffect(() => onSnapshot(collection(db, "departments"), (snapshot) => 
    setDepartments(snapshot.docs.map((doc) => ({...doc.data()})))
    ), []);

    const changeView = (id) => {
        console.log(id)
    }

    return(
        <div>
            <Header />
            <WelcomeRemark />
            <Departments departments={departments} changeView={changeView}  />
        </div>
    )
}

export default Home;