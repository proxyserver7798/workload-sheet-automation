import React, { useState } from "react";
import "../../css/stuff.css";
import picz from "../../resources/images/profiles/profile.png";
import Courses from "./stuffSections/Courses";

import { BrowserRouter, Route } from "react-router-dom";
import Coordinating from "./stuffSections/Coordinating";
import Supervising from "./stuffSections/Supervising";
import Community from "./stuffSections/Community";

const Stuff = () => {
  const [param, setParam] = useState({
    courses: true,
    coordinate: false,
    supersiving: false,
    community: false,
  });

  return (
    <div className="stuff-container">
      <div className="stuff-side-pane">
        <div className="upper-content">
          <div className="stuff-profile">
            <img src={picz} alt="pro" />
            <p>Richard Mutambisi</p>
          </div>
          <div className="stuff-btn">
            <div
              className="courses-btn stuff-btns"
              onClick={() =>
                setParam({
                  courses: true,
                  coordinate: false,
                  supersiving: false,
                  community: false,
                })
              }
            >
              <p>Courses</p>
              <label>4</label>
            </div>

            <div
              className="coordinate-btn stuff-btns"
              onClick={() =>
                setParam({
                  courses: false,
                  coordinate: true,
                  supersiving: false,
                  community: false,
                })
              }
            >
              <p>Coordinating</p>
              <label>1</label>
            </div>

            <div
              className="supervising-btn stuff-btns"
              onClick={() =>
                setParam({
                  courses: false,
                  coordinate: false,
                  supersiving: true,
                  community: false,
                })
              }
            >
              <p>Supervising</p>
              <label>0</label>
            </div>

            <div
              className="community-btn stuff-btns"
              onClick={() =>
                setParam({
                  courses: false,
                  coordinate: false,
                  supersiving: false,
                  community: true,
                })
              }
            >
              <p>Community</p>
              <label>0</label>
            </div>
          </div>
        </div>
        <div className="lower-content">
          <button>Log out</button>
        </div>
      </div>
      {param.courses ? (
        <Courses />
      ) : param.coordinate ? (
        <Coordinating />
      ) : param.supersiving ? (
        <Supervising />
      ) : (
        <Community />
      )}
    </div>
  );
};

export default Stuff;
