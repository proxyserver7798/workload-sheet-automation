import React, { useState } from "react";
import "../../css/stuff.css";
import picz from "../../resources/images/profiles/profile.png";
import Courses from "./stuffSections/Courses";

import { BrowserRouter, Route } from "react-router-dom";
import Coordinating from "./stuffSections/Coordinating";
import Supervising from "./stuffSections/Supervising";
import Community from "./stuffSections/Community";

const Hod = () => {
  const [param, setParam] = useState({
    courses: true,
    coordinate: false,
    supersiving: false,
    community: false,
  });

  return (
    <div className="stuff-container">
      <div className="stuff-side-pane">
        <div className="upper-content">
          <div className="stuff-profile">
            <img src={picz} alt="pro" />
            <p>Richard Mutambisi</p>
          </div>
          <div className="stuff-btn">
            <div
              className="courses-btn stuff-btns"
              onClick={() =>
                setParam({
                  courses: true,
                  coordinate: false,
                  supersiving: false,
                  community: false,
                })
              }
            >
              <p>Assign Roles</p>
            </div>

            <div
              className="coordinate-btn stuff-btns"
              onClick={() =>
                setParam({
                  courses: false,
                  coordinate: true,
                  supersiving: false,
                  community: false,
                })
              }
            >
              <p>Stuff</p>
            </div>

            <div
              className="supervising-btn stuff-btns"
              onClick={() =>
                setParam({
                  courses: false,
                  coordinate: false,
                  supersiving: true,
                  community: false,
                })
              }
            >
              <p>Approve Applications</p>
              <label>0</label>
            </div>
          </div>
        </div>
        <div className="lower-content">
          <button>Log out</button>
        </div>
      </div>
      {param.courses ? (
        <Courses />
      ) : param.coordinate ? (
        <Coordinating />
      ) : (
        <Community />
      )}
    </div>
  );
};

export default Hod;
