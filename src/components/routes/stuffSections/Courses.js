import React, { useState, useEffect } from "react";
import { collection, onSnapshot } from "@firebase/firestore";

import db from "../../../firebase";

const Courses = () => {
  const [courses, setCourses] = useState([]);

  useEffect(
    () =>
      onSnapshot(collection(db, "courses"), (snapshot) =>
        setCourses(snapshot.docs.map((doc) => ({ ...doc.data() })))
      ),
    []
  );

  console.log(courses);

  return (
    <div className="stuff-main-content">
      <div className="sec-titles">
        <h3>Courses</h3>
        <p>These are all the courses you are teaching</p>
      </div>
      <div className="styledClasses">
        {courses.map((course) => {
          return (
            <div className="act-courses">
              <div className="m-course">
                <p className="cCode"> {course.code} </p>
                <p className="cName"> {course.name} </p>
                <div className="c-group">
                  <div className="theory-rep">
                    <div className="theory-num">1</div>
                    <div className="theory-title">Theory(s)</div>
                  </div>

                  <div className="prac-rep">
                    <div className="prac-num">3</div>
                    <div className="prac-title">Practical(s)</div>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      <div className="num-of-courses">
        <div className="prac-courses">
          <label>3</label>
          <p>Practical(s)</p>
        </div>
        <div className="theo-courses">
          <label>1</label>
          <p>Theory(s)</p>
        </div>
      </div>
    </div>
  );
};

export default Courses;
