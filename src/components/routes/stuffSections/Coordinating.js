import React, { useState } from "react";

import "../../../css/stuff.css";

const Coordinating = () => {
  const [coordinate, setCoordinate] = useState(false);

  return (
    <div>
      {coordinate ? (
        <div className="stuff-main-content">
          <div className="act-courses">
            <div className="m-course">
              <p className="cCode">MCI521S</p>
              <p className="cName">Mathematics for computing</p>
              <div className="c-group">
                <div className="theory-rep">
                  <div className="theory-num">1</div>
                  <div className="theory-title">Theory(s)</div>
                </div>

                <div className="prac-rep">
                  <div className="prac-num">3</div>
                  <div className="prac-title">Practical(s)</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div>
          <h1>You are not coordinating any course</h1>
        </div>
      )}
    </div>
  );
};

export default Coordinating;
