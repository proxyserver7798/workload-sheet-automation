import "../css/auth.css";

import { collection, onSnapshot } from "@firebase/firestore";
import React, { useState, useEffect } from "react";
import { BrowserRouter, Route, Link } from "react-router-dom";

import profile from "../resources/images/profiles/profile.png";
import gIcon from "../resources/images/icons/google.png";

import db from "../firebase";

const AuthenicationView = () => {
  const [users, setUsers] = useState([]);

  useEffect(
    () =>
      onSnapshot(collection(db, "users"), (snapshot) =>
        setUsers(snapshot.docs.map((doc) => ({ ...doc.data() })))
      ),
    []
  );

  const [gUsername, setGusername] = useState("");
  const [gPassword, setGpassword] = useState("");

  const [destiny, setDestiny] = useState("/");
  const [accessDisplay, setAccessDisplay] = useState(false);

  const [error, setError] = useState({
    gMessage: "Sign in to your account",
    bMessage: "Access Denied, Try again",
    isCorrect: true,
  });

  const authProcess = (e) => {
    e.preventDefault();
    const expUser = users.find((user) => {
      return user.email === gUsername && user.password === gPassword;
    });
    if (expUser) {
      if (expUser.id == 1) {
        setDestiny("/admin");
        setAccessDisplay(true);
      } else if (expUser.id == 2) {
        setDestiny("/hod");
        setAccessDisplay(true);
      } else if (expUser.id == 3) {
        setDestiny("/dvc");
        setAccessDisplay(true);
      } else if (expUser.id == 4) {
        setDestiny("/stuff");
        setAccessDisplay(true);
      }
    } else {
      console.log("Try again");
      setError({ ...error, isCorrect: !error.isCorrect });
    }
    setGusername("");
    setGpassword("");
  };

  return (
    <div className="authContainer">
      <img src={profile} alt="sec" />
      <h3>WORKLOAD SHEET AUTOMATOR</h3>
      <p>{error.isCorrect ? error.gMessage : error.bMessage}</p>
      <form onSubmit={(e) => authProcess(e)}>
        <input
          type="text"
          placeholder="Enter your email address"
          onChange={(event) => setGusername(event.target.value)}
          value={gUsername}
        />
        <input
          type="text"
          placeholder="Enter your email password"
          onChange={(event) => setGpassword(event.target.value)}
          value={gPassword}
        />
        <button className="gButton" type="submit">
          Sign in
        </button>
      </form>
      <div>
        <div className="googleLBtn">
          <img src={gIcon} alt="sec" />
          <p>Log in with google</p>
        </div>
      </div>
      <div
        className="access-granted"
        style={{ display: accessDisplay ? "flex" : "none" }}
      >
        <div className="access-granted-box">
          <h3>Access Granted</h3>
          <Route>
            <Link
              to={destiny}
              className="accessBtn"
              onClick={() => setAccessDisplay(false)}
            >
              Continue
            </Link>
          </Route>
        </div>
      </div>
    </div>
  );
};

export default AuthenicationView;
