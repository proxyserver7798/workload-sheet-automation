import React from "react";
import "../css/departments.css";
import profile from "../resources/images/profiles/profile.png";
import { Link } from "react-router-dom";
import { Route } from "react-router";

const Departments = ({ departments, changeView }) => {
  // const [ departments, setDepartments ] = useState([
  //     {
  //         id: 1,
  //         profile: profile,
  //         banner: "admin-banner",
  //         userType: "admin",
  //         button: "proceed",
  //     },
  // ])

  const authProceed = (id) => {
    changeView(id);
  };

  return (
    <div className="dep-container">
      {departments.map((dep) => {
        return (
          <div className={`${dep.userType} depBox`} key={dep.id}>
            <img src={profile} alt="profile" />
            <h3>{dep.userType}</h3>
            <Route>
              <button className="depBtn" onClick={() => authProceed(dep.id)}>
                <Link className="depBtnInner" to="auth">
                  Proceed
                </Link>
              </button>
            </Route>
            <div className="backCorrector"></div>
          </div>
        );
      })}
    </div>
  );
};

export default Departments;
