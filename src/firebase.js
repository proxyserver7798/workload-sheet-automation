// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyA_W9Ue5UFm9SBfKa_8-sN6S9ZiPvGjd5E",
  authDomain: "workload-sheet-automation.firebaseapp.com",
  projectId: "workload-sheet-automation",
  storageBucket: "workload-sheet-automation.appspot.com",
  messagingSenderId: "845371283503",
  appId: "1:845371283503:web:6ccbb74a18dd8c6eca8eda"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export default getFirestore();