import { collection, onSnapshot } from '@firebase/firestore';
import React, { useState, useEffect } from 'react';
import { BrowserRouter, Link, Route } from 'react-router-dom';

import './App.css';
import AuthenticationView from './components/AuthenticationView';
import Departments from './components/Departments';
import Header from './components/Header';
import WelcomeRemark from './components/WelcomeRemark';

import Stuff from './components/routes/Stuff';
import Home from './components/Home';

import db from "./firebase";

function App() {

  // const [ departments, setDepartments ] = useState([]);

  // console.log(departments);

  // useEffect(() => onSnapshot(collection(db, "departments"), (snapshot) => 
  //   setDepartments(snapshot.docs.map((doc) => ({...doc.data()})))
  //   ), []);

  //   const [ view, setView ] = useState(true);

  //   const changeView = (id) => {
  //     setView(false);
  //     console.log(id)
  //   }
  
  return (
    <div className="App">
      {/* {
        view ? (
          <div>
            <Header />
            <WelcomeRemark />
            <Departments departments={departments} changeView={changeView} /> 
          </div>
        ) : (
          <div className="authinapp">
            
            <BrowserRouter>
                <Route exact path="/" component={AuthenticationView} />
                <Route path="/stuff" component={Stuff} />
            </BrowserRouter>
          </div>
        )
      } */}

      <BrowserRouter>
          <Route exact path="/" component={Home} />
      </BrowserRouter>
    </div>
  );
}

export default App;
